﻿using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Net.Client;
using ProtoClient.Abstractions;

// The port number must match the port of the gRPC server.
using var channel = GrpcChannel.ForAddress("https://localhost:7005");

var clientUsers = new UsersService.UsersServiceClient(channel);
var clientTask = new TasksService.TasksServiceClient(channel);

try
{
    var user = await clientUsers.LoginAsync(new LoginInfo()
    {
        Login = "test_user"
    });

    var task = await clientTask.AddNewTaskAsync(new TaskInfo()
    {
        Title = "Сделать БД",
        UserId = user.Id
    });

    task = await clientTask.AddNewTaskAsync(new TaskInfo()
    {
        Title = "Сделать логирование и сервисы",
        UserId = user.Id
    });

    var tasks = await clientTask.GetUserTaskListAsync(new TaskListRequest()
    {
        UserId = user.Id
    });

    Console.WriteLine($"Количество тасок юзера {user.User.Login}: {tasks.Tasks.Count}");
    foreach (var fullTaskInfo in tasks.Tasks)
    {
        Console.WriteLine($"title: {fullTaskInfo.Task.Title}");
    }
}
catch (Exception e)
{
    Console.WriteLine(e);
    throw;
}

Console.WriteLine("Press any key to exit...");
Console.ReadKey();