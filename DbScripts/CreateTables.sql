COMMENT ON DATABASE todo_db
    IS 'DB for ToDo project (prototype)';

CREATE TABLE users
(
    Id SERIAL PRIMARY KEY,
    Login VARCHAR(50) NOT NULL,
    Email VARCHAR(100) NOT NULL
);

INSERT INTO users (Login, Email)
values ('test_user', 'test_user@todo.ru');

CREATE TABLE taskstatuses
(
	Id SMALLSERIAL PRIMARY KEY,
	Description varchar(50) NOT NULL
);

INSERT INTO TaskStatuses (Description)
VALUES ('Открыто'), ('В процессе'), ('Закрыто');

CREATE TABLE tasks
(
	Id SERIAL PRIMARY KEY,
	Title varchar(100) NOT NULL,
	Description varchar(500) DEFAULT NULL,
	StatusId SMALLINT REFERENCES TaskStatuses (Id),
	UserId INTEGER REFERENCES Users (Id)
);