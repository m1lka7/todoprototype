## Инструкция по запуску сервера:

Собрать и запустить конфигурацию Debug проекта ToDoServerPrototype. Откроется консоль с примерно следующим выводом в начале окна:

`````
        info: Microsoft.Hosting.Lifetime[14]
            Now listening on: https://localhost:59817
        info: Microsoft.Hosting.Lifetime[14]
            Now listening on: http://localhost:59818
        info: Microsoft.Hosting.Lifetime[0]
            Application started. Press Ctrl+C to shut down.
`````

## Как правильно запустить клиент:
1. При создании канала gRPC нужно указать верный адрес в строке:

        using var channel = GrpcChannel.ForAddress("https://localhost:59817");
		

2. Можно собирать и запускать проект.

Обратите внимание, что адрес сервера по протоколу https и адрес, которым инициализируется канал - одинаковые.

## Для проекта требуется .Net 6.

## Установка и настройка БД PostgreSQL

1. С сайта https://www.enterprisedb.com/downloads/postgres-postgresql-downloads скачиваем последнюю версию для вашей ОС;
2. Устанавливаем скачавщийся файл:
	— Компоненты выбираем все, среди них сам сервер и pgAdmin для администрирования БД;
	— При установке задаем пароль суперюзера;
	— Задаем порт, на котором будет поднят сервер на вашем ПК;
	— Локаль советую выбрать Russian/Russia.
3. После установки - запускаем pgAdmin:
	— Вводим заданный пароль от супер пользователя;
4. Создаем БД:
	— раскрываем PostgreSQL (версия), Databases, postgres и жмем ПКМ, выбираем Query tool;
	— вставляем содержимое скрипта DbScripts\CreateDb.sql и выполняем;
	— жмем ПКМ на Databases и выбираем Refresh;
5. Наполняем таблицами и прочим:
	— В Query toos сверху в подключениях выбираем New connections и выбираем только что созданную БД todo_db;
	— Берем содержимое скрипта DbScripts\CreateTables.sql и вставляем в окно. Выполняем.

## Запуск сервера с подключением к БД
1. Идем в файл appsettings.Development.json, пишем в полях базы порт, юзернейм и пароль (можно в pgAdmin завести нового и глянуть порт, под которым поднят сервак)
2. Запускаем сервер и идем по методу get_users. Если нет ошибки и вернулось 1 - все работает.

## Как завести юзера для предыдущего абзаца?
1. Идем в pgAdmin, раскрываем сервер;
2. ПКМ по Login/Group Roles -> Create - Login/Group roles
3. Забиваем имя юзера и пароль;
4. Роли суперюзер (пока что)
5. Данные от него указываем в json в имени и пароле.