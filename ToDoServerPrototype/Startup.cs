﻿using Db.Abstractions.Repository;
using Db.Common;
using Db.Postgre;
using Microsoft.AspNetCore.Identity;
using NLog.Extensions.Logging;
using NLog.Web.Targets.Wrappers;
using Proto.Impl.Tasks;
using Proto.Impl.Users;

namespace ToDoServerPrototype
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddGrpc();
            services.AddDbServices()
                .AddLogging()
                .AddLogging(q =>
                {
                    q.ClearProviders();
                    q.SetMinimumLevel(LogLevel.Trace);
                    q.AddNLog();
                });

            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional:false, reloadOnChange:true)
                .AddJsonFile("appsettings.Development.json", optional:true)
                .Build();

            var dbSettings = new DbSettings();
            config.GetSection("DbSettings").Bind(dbSettings);

            services.AddSingleton(dbSettings);
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseRouting();

            app.UseGrpcWeb(new GrpcWebOptions {DefaultEnabled = true});

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGrpcService<TaskService>().EnableGrpcWeb();
                endpoints.MapGrpcService<UserService>().EnableGrpcWeb();

                endpoints.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
                endpoints.MapGet("/get_users", async (IUsersRepository u) => (await u.GetAllAsync()).First().Login);
            });
        }
    }
}
