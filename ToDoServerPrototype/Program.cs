using Db.Common;
using ToDoServerPrototype;

var builder = Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(wb =>
{
    wb.UseStartup<Startup>();
});


var app = builder.Build();

app.Run();
