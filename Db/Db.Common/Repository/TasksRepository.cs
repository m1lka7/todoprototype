﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Db.Abstractions.Models;
using Db.Abstractions.Repository;
using Microsoft.EntityFrameworkCore;

namespace Db.Common.Repository
{
    public class TasksRepository<TContext> : RepositoryBase<TaskToDo, TContext>, ITasksRepository
        where TContext : DbContext
    {
        private readonly TContext _context;

        public TasksRepository(TContext context) 
            : base(context)
        {
            _context = context;
        }

        public Task<List<TaskToDo>> GetAllUserTasks(int userId)
        {
            return FindAllByWhereAsync(task => task.UserId == userId);
        }
    }
}
