﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Db.Abstractions.Models;
using Db.Abstractions.Repository;
using Microsoft.EntityFrameworkCore;

namespace Db.Common.Repository
{
    public class UsersRepository<TContext>: RepositoryBase<User, TContext>, IUsersRepository
        where TContext: DbContext
    {
        private readonly TContext _context;

        public UsersRepository(TContext context) 
            : base(context)
        {
            _context = context;
        }

        public Task<User?> GetUserByLoginAsync(string login)
        {
            return _context.Set<User>()
                .Where(user => user.Login == login)
                .FirstOrDefaultAsync();
        }

        public Task<User> AddUserAsync(User user)
        {
            user.Id = 0;
            return InsertAsync(user);
        }

        public async Task<bool> CheckUserExistsAsync(string login)
        {
            return await CountWhere(user => user.Login.Equals(login)) > 0;
        }
    }
}
