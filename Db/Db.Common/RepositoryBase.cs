﻿using Db.Abstractions.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Db.Common
{
    public abstract class RepositoryBase<TEntity, TContext> : IRepositoryBase<TEntity> where TEntity : class
        where TContext : DbContext
    {
        public TContext Context { get; }

        public IQueryable<TEntity> DbSet => Context.Set<TEntity>();

        protected RepositoryBase(TContext context)
        {
            Context = context;
        }

        public void Delete(TEntity entity)
        {
            Context.ChangeTracker.Clear();

            Context.Set<TEntity>().Remove(entity);
            Context.SaveChanges();
        }

        public void DeleteRange(ICollection<TEntity> entities)
        {
            Context.ChangeTracker.Clear();

            Context.Set<TEntity>().RemoveRange(entities);
            Context.SaveChanges();
        }

        public async Task<int> Count()
        {
            return await Context.Set<TEntity>().AsNoTracking().CountAsync();
        }

        public virtual async Task<List<TEntity>> GetAllAsync()
        {
            return await Context.Set<TEntity>().AsNoTracking().ToListAsync();
        }

        public async Task<List<TEntity>> FindAllByWhereAsync(Expression<Func<TEntity, bool>> match)
        {
            return await Context.Set<TEntity>().AsNoTracking().Where(match).ToListAsync();
        }

        /// <inheritdoc />
        public async Task<int> CountWhere(Expression<Func<TEntity, bool>> match)
        {
            return await Context.Set<TEntity>().AsNoTracking().Where(match).CountAsync();
        }

        public async Task<List<TEntity>> FindAllByWhereOrderedAscendingAsync(
            Expression<Func<TEntity, bool>> match,
            Expression<Func<TEntity, object>> orderBy)
        {
            return await Context.Set<TEntity>().AsNoTracking().Where(match).OrderBy(orderBy)
                .ToListAsync();
        }

        public async Task<List<TEntity>> FindAllByWhereOrderedDescendingAsync(
            Expression<Func<TEntity, bool>> match,
            Expression<Func<TEntity, object>> orderBy)
        {
            return await Context.Set<TEntity>().AsNoTracking().Where(match)
                .OrderByDescending(orderBy)
                .ToListAsync();
        }

        public async Task<bool> AnyAsync(Expression<Func<TEntity, bool>> match)
        {
            return await Context.Set<TEntity>().AsNoTracking().AnyAsync(match);
        }

        /// <inheritdoc />
        public async Task<int> CountAsyncIQueryable(IQueryable<TEntity> queryable)
        {
            return await queryable.CountAsync();
        }

        /// <inheritdoc />
        public async Task<int> CountAsyncIQueryable(IQueryable<TEntity> queryable,
            Expression<Func<TEntity, bool>> match)
        {
            return await queryable.CountAsync(match);
        }

        /// <inheritdoc />
        public async Task<List<TEntity>> MaterializeIQueryable(IQueryable<TEntity> existQueryable)
        {
            return await existQueryable.ToListAsync();
        }

        /// <inheritdoc />
        public async Task<List<T>> MaterializeIQueryable<T>(IQueryable<T> existQueryable)
        {
            return await existQueryable.ToListAsync();
        }

        public virtual async Task<TEntity?> GetFirstWhereAsync(Expression<Func<TEntity?, bool>> match)
        {
            return await Context.Set<TEntity>().AsNoTracking().FirstOrDefaultAsync(match);
        }

        public virtual async Task<TEntity> UpdateAsync(TEntity entityToUpdate)
        {
            Context.ChangeTracker.Clear();
            if (Context.Entry(entityToUpdate).State == EntityState.Detached)
            {
                Context.Set<TEntity>().Attach(entityToUpdate);
            }

            Context.Entry(entityToUpdate).State = EntityState.Modified;
            Context.ChangeTracker.AutoDetectChangesEnabled = false;
            await Context.SaveChangesAsync();
            Context.ChangeTracker.Clear();
            return entityToUpdate;
        }

        public virtual async Task<IList<TEntity>> UpdateRangeAsync(IList<TEntity> entities)
        {
            Context.ChangeTracker.Clear();
            var detachedEntities = new List<TEntity>();
            Context.ChangeTracker.AutoDetectChangesEnabled = false;
            foreach (var entity in entities)
            {
                if (Context.Entry(entity).State == EntityState.Detached)
                {
                    detachedEntities.Add(entity);
                }

                Context.Entry(entity).State = EntityState.Modified;
            }

            Context.Set<TEntity>().AttachRange(detachedEntities);
            await Context.SaveChangesAsync();
            Context.ChangeTracker.AutoDetectChangesEnabled = true;
            foreach (var entity in detachedEntities)
            {
                Context.Entry(entity).State = EntityState.Detached;
            }

            return entities;
        }

        public virtual async Task<TEntity> InsertAsync(TEntity entity)
        {
            try
            {
                Context.ChangeTracker.Clear();
                await Context.Set<TEntity>().AddAsync(entity);
                await Context.SaveChangesAsync();
            }
            finally
            {
                Context.ChangeTracker.Clear();
            }

            return entity;
        }

        public virtual async Task<IList<TEntity>> InsertRangeAsync(IList<TEntity> entities, bool saveChanges = true)
        {
            await Context.Set<TEntity>().AddRangeAsync(entities);
            if (saveChanges)
            {
                await Context.SaveChangesAsync();
            }

            return entities;
        }
    }

}
