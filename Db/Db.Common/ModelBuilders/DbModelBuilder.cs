﻿using Db.Abstractions.Models;
using Microsoft.EntityFrameworkCore;

namespace Db.Common.ModelBuilders
{
    public static class DbModelBuilder
    {
        public static void BuildDbModels(this ModelBuilder modelBuilder)
        {
            modelBuilder.AppendUserTable()
                .AppendTasksTable();
        }

        private static ModelBuilder AppendUserTable(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(table =>
            {
                table.HasKey(user => user.Id);

                table.ToTable("users");

                table.Property(user => user.Id).HasColumnName("id")
                    .HasColumnType("SERIAL");

                table.Property(user => user.Login).HasColumnName("login")
                    .HasColumnType("varchar(50)").HasMaxLength(50).IsRequired();

                table.Property(user => user.Email).HasColumnName("email")
                    .HasColumnType("varchar(100)").HasMaxLength(100).IsRequired();
            });

            return modelBuilder;
        }

        private static ModelBuilder AppendTasksTable(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TaskToDo>(table =>
            {
                table.HasKey(task => task.Id);

                table.ToTable("tasks");

                table.Property(task => task.Id).HasColumnName("id")
                    .HasColumnType("SERIAL");

                table.Property(task => task.Title).HasColumnName("title")
                    .HasColumnType("varchar(100)").HasMaxLength(100).IsRequired();

                table.Property(task => task.Description).HasColumnName("description")
                    .HasColumnType("varchar(500)").HasMaxLength(500);

                table.Property(task => task.Status).HasColumnName("statusid")
                    .HasColumnType("smallint").IsRequired();

                table.Property(task => task.UserId).HasColumnName("userid")
                    .HasColumnType("integer").IsRequired();

                // TODO сделать связи с юзерами
            });

            return modelBuilder;
        }
    }
}
