﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Db.Abstractions.Models;

namespace Db.Abstractions.Repository
{
    /// <summary>
    /// Репозиторий для работы с таблицей задач
    /// </summary>
    public interface ITasksRepository: IRepositoryBase<TaskToDo>
    {
        Task<List<TaskToDo>> GetAllUserTasks(int userId);
    }
}
