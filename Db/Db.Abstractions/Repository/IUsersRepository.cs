﻿using System.Threading.Tasks;
using Db.Abstractions.Models;

namespace Db.Abstractions.Repository
{

    /// <summary>
    /// Репозиторий для работы с таблицей пользователей (users)
    /// </summary>
    public interface IUsersRepository: IRepositoryBase<User>
    {
        Task<User?> GetUserByLoginAsync(string login);

        Task<User> AddUserAsync(User user);

        Task<bool> CheckUserExistsAsync(string login);
    }
}
