﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Db.Abstractions.Repository
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        IQueryable<TEntity> DbSet { get; }

        /// <summary>
        /// Получить первую запись, которая соответствует фильтру
        /// </summary>
        /// <param name="match">выражение</param>
        /// <returns>модель (сущность)</returns>
        Task<TEntity?> GetFirstWhereAsync(Expression<Func<TEntity?, bool>> match);

        /// <summary>
        /// Получить все записи, которые соответствуют фильтру
        /// </summary>
        /// <param name="match">выражение</param>
        /// <returns>список моделей (сущностей)</returns>
        Task<List<TEntity>> FindAllByWhereAsync(Expression<Func<TEntity, bool>> match);

        /// <summary>
        /// Получить кол-во записей, которые соответствуют фильтру
        /// </summary>
        /// <param name="match">выражение</param>
        /// <returns>количество</returns>
        Task<int> CountWhere(Expression<Func<TEntity, bool>> match);

        /// <summary>
        /// Получить все записи из таблицы
        /// </summary>
        /// <returns>список моделей (сущностей)</returns>
        Task<List<TEntity>> GetAllAsync();

        /// <summary>
        /// Обновить список моделей в БД
        /// </summary>
        /// <param name="entities">сущности к обновлению</param>
        /// <returns>список обновленных сущностей</returns>
        Task<IList<TEntity>> UpdateRangeAsync(IList<TEntity> entities);

        /// <summary>
        /// Обновить 1 сущность
        /// </summary>
        /// <param name="entityToUpdate">сущность</param>
        /// <returns>обновленная сущность</returns>
        Task<TEntity> UpdateAsync(TEntity entityToUpdate);

        /// <summary>
        /// Вставить новую запись в таблицу
        /// </summary>
        /// <param name="entity">сущность</param>
        /// <returns>вставленная сущность</returns>
        Task<TEntity> InsertAsync(TEntity entity);

        /// <summary>
        /// Вставить список записей в таблицу
        /// </summary>
        /// <param name="entities">список сущностей</param>
        /// <param name="saveChanges">хз</param>
        /// <returns>список вставленных сущностей</returns>
        Task<IList<TEntity>> InsertRangeAsync(IList<TEntity> entities, bool saveChanges = true);

        /// <summary>
        /// Удалить сущность
        /// </summary>
        /// <param name="entity">сущность к удалению</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Удалить список сущностей
        /// </summary>
        /// <param name="entities">список сущностей к удалению</param>
        void DeleteRange(ICollection<TEntity> entities);

        /// <summary>
        /// Получить кол-во записей в таблице
        /// </summary>
        /// <returns></returns>
        Task<int> Count();

        Task<bool> AnyAsync(Expression<Func<TEntity, bool>> match);

        Task<int> CountAsyncIQueryable(IQueryable<TEntity> queryable);

        Task<int> CountAsyncIQueryable(IQueryable<TEntity> queryable, Expression<Func<TEntity, bool>> match);

        Task<List<TEntity>> MaterializeIQueryable(IQueryable<TEntity> existQueryable);

        Task<List<T>> MaterializeIQueryable<T>(IQueryable<T> existQueryable);
    }
}
