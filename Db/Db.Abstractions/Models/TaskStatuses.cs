﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Abstractions.Models
{
    public enum TaskStatuses
    {
        Open = 1,
        InProgress = 2,
        Closed = 3
    }
}
