﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Abstractions.Models
{
    public class TaskToDo
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Description { get; set; }

        public TaskStatuses Status { get; set; }

        public int UserId { get; set; }

        public User? User { get; set; }
    }
}
