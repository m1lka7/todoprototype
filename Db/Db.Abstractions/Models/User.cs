﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Db.Abstractions.Models
{
    public class User
    {
        public int Id { get; set; }

        public string Login { get; set; }

        public string Email { get; set; }
    }
}
