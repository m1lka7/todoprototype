﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Db.Abstractions.Repository;
using Db.Common.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace Db.Postgre
{
    public static class Di
    {
        public static IServiceCollection AddDbServices(this IServiceCollection services)
        {
            return services.AddDbContext<PostgreDbContext>()
                .AddScoped<IUsersRepository, UsersRepository<PostgreDbContext>>()
                .AddScoped<ITasksRepository, TasksRepository<PostgreDbContext>>();
        }
    }
}
