﻿using Db.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Db.Common.ModelBuilders;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace Db.Postgre
{
    internal class PostgreDbContext: DbContext
    {
        private readonly ILoggerFactory _loggerFactory;
        private readonly DbSettings _settings;

        public PostgreDbContext(ILoggerFactory loggerFactory, DbSettings dbSettings)
        {
            _loggerFactory = loggerFactory;
            _settings = dbSettings;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseLoggerFactory(_loggerFactory);

            optionsBuilder.UseNpgsql(new NpgsqlConnectionStringBuilder()
            {
                Host = _settings.Host,
                Port = Int32.Parse(_settings.Port),
                Database = _settings.Database,
                Username = _settings.Username,
                Password = _settings.Password
            }.ToString());
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.BuildDbModels();
        }
    }
}
