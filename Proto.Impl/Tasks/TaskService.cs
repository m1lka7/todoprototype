﻿using Db.Abstractions.Models;
using Db.Abstractions.Repository;
using Google.Protobuf.Collections;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Proto.Abstractions;

namespace Proto.Impl.Tasks
{
    public class TaskService : TasksService.TasksServiceBase
    {
        private readonly ILogger<TaskService> _logger;
        private readonly ITasksRepository _tasksRepository;

        public TaskService(ILogger<TaskService> logger, ITasksRepository tasksRepository)
        {
            _logger = logger;
            _tasksRepository = tasksRepository;
        }

        public override async Task<FullTaskInfo> AddNewTask(TaskInfo request, ServerCallContext context)
        {
            using (_logger.BeginScope(nameof(AddNewTask)))
            {
                _logger.LogInformation($"Добавляем новую таску для юзера {request.UserId} с заголовком: {request.Title}");

                var addedTask = await _tasksRepository.InsertAsync(new TaskToDo()
                {
                    Title = request.Title,
                    Description = request.Description,
                    UserId = request.UserId,
                    Status = TaskStatuses.Open
                });

                return new FullTaskInfo()
                {
                    Id = addedTask.Id,
                    Status = (TaskState)addedTask.Status,
                    Task = request
                };
            }
        }

        public override async Task<TaskList> GetUserTaskList(TaskListRequest request, ServerCallContext context)
        {
            using (_logger.BeginScope(nameof(GetUserTaskList)))
            {
                _logger.LogInformation($"Получаем все таски для юзера {request.UserId}");

                var taskList = await _tasksRepository.GetAllUserTasks(request.UserId);

                return new TaskList()
                {
                    Tasks = {taskList.ConvertAll(q => new FullTaskInfo()
                    {
                        Id = q.Id,
                        Status = (TaskState)q.Status,
                        Task = new TaskInfo()
                        {
                            Title = q.Title,
                            Description = q.Description,
                            UserId = q.UserId
                        }
                    })}
                };
            }
        }
    }
}
