﻿using Db.Abstractions.Models;
using Db.Abstractions.Repository;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using Proto.Abstractions;
using Proto.Impl.Exceptions.Users;

namespace Proto.Impl.Users
{
    public class UserService : UsersService.UsersServiceBase
    {
        private readonly ILogger<UserService> _logger;
        private readonly IUsersRepository _userRepository;

        public UserService(ILogger<UserService> logger, IUsersRepository userRepository)
        {
            _logger = logger;
            _userRepository = userRepository;
        }

        public override async Task<FullUserInfo> RegisterNewUser(UserInfo request, ServerCallContext context)
        {
            var login = request.Login;

            using (_logger.BeginScope(nameof(RegisterNewUser)))
            {
                _logger.LogInformation($"Регистрируем пользователя логин: {login}, email: {request.Email}");

                if (await _userRepository.CheckUserExistsAsync(login))
                    throw new UserExistsException(login);

                var user = await _userRepository.AddUserAsync(new User()
                {
                    Login = login,
                    Email = request.Email
                });

                return new FullUserInfo()
                {
                    Id = user.Id,
                    User = new UserInfo()
                    {
                        Login = login,
                        Email = user.Email
                    }
                };
            }
        }

        public override async Task<FullUserInfo> Login(LoginInfo request, ServerCallContext context)
        {
            var login = request.Login;

            using (_logger.BeginScope(nameof(Login)))
            {
                _logger.LogInformation($"Авторизуем пользователя с логином: {login}");

                var user = await _userRepository.GetUserByLoginAsync(login);

                if (user is not null)
                    return new FullUserInfo()
                    {
                        Id = user.Id,
                        User = new UserInfo()
                        {
                            Login = login,
                            Email = user.Email
                        }

                    };

                throw new UserNotRegisteredException(login);
            }
        }
    }
}
