﻿using Grpc.Core;

namespace Proto.Impl.Exceptions.Users
{
    public class UserNotRegisteredException : RpcException
    {
        public UserNotRegisteredException(string login)
            : base(new Status(StatusCode.NotFound, GenerateMessage(login)))
        { }

        public UserNotRegisteredException(string login, string message)
            : base(new Status(StatusCode.NotFound, GenerateMessage(login)), message)
        {
        }

        public UserNotRegisteredException(Status status, Metadata trailers) : base(status, trailers)
        {
        }

        public UserNotRegisteredException(Status status, Metadata trailers, string message) : base(status, trailers, message)
        {
        }

        private static string GenerateMessage(string login) =>
            $"Пользователь с логином {login} не зарегистрирован в системе!";
    }
}
