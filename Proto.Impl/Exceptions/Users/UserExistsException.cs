﻿using Grpc.Core;

namespace Proto.Impl.Exceptions.Users
{
    public class UserExistsException : RpcException
    {
        public UserExistsException(string login)
            : base(new Status(StatusCode.AlreadyExists, GenerateMessage(login)))
        { }

        public UserExistsException(string login, string message)
            : base(new Status(StatusCode.AlreadyExists, GenerateMessage(login)), message)
        {
        }

        public UserExistsException(Status status, Metadata trailers) : base(status, trailers)
        {
        }

        public UserExistsException(Status status, Metadata trailers, string message) : base(status, trailers, message)
        {
        }

        private static string GenerateMessage(string login) =>
            $"Пользователь с логином {login} уже зарегистрирован!";
    }
}
